"use strict";

import Vue from 'vue'
import axios from "axios";
import Buefy from 'buefy'
import moment from "moment";
import 'moment/locale/fi';
import 'buefy/lib/buefy.css'

Vue.use(Buefy);
window.axios = axios;
window.moment = moment;


Vue.component('reports', require('./components/Reports.vue'));

const app = new Vue({
    el: '#app'
});
