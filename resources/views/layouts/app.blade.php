<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Turva</title>
  <!-- Styles -->
  <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="//cdn.materialdesignicons.com/2.0.46/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/app.css">

</head>
<body>
<div id="app">
  <div class="container">
    @if (\Illuminate\Support\Facades\Auth::user())
      <nav class="navbar">
        <div class="navbar-item has-dropdown is-hoverable navbar-end">
          <span class="navbar-item">{{  \Illuminate\Support\Facades\Auth::user()->name }}</span>
          <div class="navbar-dropdown is-boxed">
            <!-- <hr class="navbar-divider"> -->
            <a class="navbar-item is-active"
               href="{{ route('logout') }}">
              Kirjaudu ulos
            </a>
          </div>
        </div>
      </nav>
    @endif
  </div>
  @yield('content')
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
