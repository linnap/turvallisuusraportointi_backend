<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Turva</title>
  <!-- Styles -->
  <link rel="stylesheet" href="//unpkg.com/buefy/lib/buefy.min.css">

</head>

<body>

<section class="hero">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        Kirjaudu
      </h1>
    </div>
  </div>
</section>

<div class="container">

  <form class="form-horizontal" method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}
    <div class="field">
      <label class="label">Sähköposti</label>
      <div class="control">
        <input class="input {{ $errors->has('email') ? ' is-danger' : '' }}" type="email" name="email"
               value="{{ old('email') }}" placeholder="Sähköposti">
      </div>
      @if ($errors->has('email'))
        <span class="help is-danger">
     <strong>{{ $errors->first('email') }}</strong>
    </span>
      @endif
    </div>

    <div class="field">
      <label class="label">Salasana</label>
      <div class="control">
        <input class="input {{ $errors->has('password') ? ' is-danger' : '' }}" name="password" type="password"
               placeholder="Salasana">
      </div>
      @if ($errors->has('password'))
        <span class="help is-danger">
     <strong>{{ $errors->first('password') }}</strong>
    </span>
      @endif
    </div>

    <div class="field">
      <div class="control">
        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Muista minut
      </div>
    </div>

    <div class="field">
      <p class="control">
        <button type="submit" class="button is-success">
          Kirjaudu
        </button>
      </p>
    </div>

    <a class="btn btn-link" href="{{ route('password.request') }}">
      Unohditko salasanan?
    </a>
  </form>

</div>
</body>
</html>
