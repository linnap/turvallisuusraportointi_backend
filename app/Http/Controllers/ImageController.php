<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ImageController extends Controller {

  public function showThumbnail($filename) {

    $path = storage_path() . '/app/images/' . $filename;

    if (!File::exists($path)) {
      return response()->json(['message' => 'Image not found.'], 404);
    }

    //creates thumbnail directory, if not exists
    if (!is_dir(storage_path() . '/app/images/thumbnails/')) {
      File::makeDirectory(storage_path() . '/app/images/thumbnails/');
    }

    $thumb_path = storage_path() . '/app/images/thumbnails/' . $filename;

    if (!File::exists($thumb_path)) {
      $img = Image::make($path);
      $img->resize(300, NULL, function ($constraint) {
        $constraint->aspectRatio();
      });
      $img->save($thumb_path);
    }

    $file = File::get($thumb_path);
    $type = File::mimeType($thumb_path);

    return response($file, 200)->header("Content-Type", $type);
  }

  public function showFull($filename) {
    $path = storage_path() . '/app/images/' . $filename;

    if (!File::exists($path)) {
      return response()->json(['message' => 'Image not found.'], 404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    return response($file, 200)->header("Content-Type", $type);
  }
}
