<?php


namespace App\Http\Controllers;


use App\Http\Transformers\CustomSerializer;
use League\Fractal\Manager;

class APIController extends Controller {

  protected $statusCode = 200;


  protected function setStatusCode($code) {
    $this->statusCode = $code;
    return $this;
  }

  protected function getStatusCode() {
    return $this->statusCode;
  }

  protected function respond($data, $headers = []) {
    return response()->json($data, $this->getStatusCode(), $headers);
  }

  protected function respondWithError($message) {
    return $this->respond([
      'error' => [
        'message' => $message
      ]
    ]);
  }

  protected function respondNotFound($message = 'Not Found') {
    return $this->setStatusCode(404)->respondWithError($message);
  }

  protected function respondInvalidInput($message = 'Invalid Input') {
    return $this->setStatusCode(422)->respondWithError($message);
  }

  protected function respondFractal($resource, array $includes = []) {
    $manager = new Manager();
    $manager->setSerializer(new CustomSerializer());

    if (sizeof($includes) == 0) {
      $data = $manager->createData($resource)
        ->toArray();
    }
    else {
      $manager->parseIncludes($includes);
      $data = $manager->createData($resource)
        ->toArray();
    }

    return $this->respond(['data' => $data]);
  }

}