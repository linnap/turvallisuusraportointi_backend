<?php

namespace App\Http\Controllers;

use App\Http\Transformers\ReportTransformer;
use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use League\Fractal\Resource\Collection;

class ReportController extends APIController {
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $resource = new Collection(Report::all(), new ReportTransformer);
    return $this->respondFractal($resource);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create() {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $report_data = $request->post();
    if (!$report_data['audio']) {
      unset($report_data['audio']);
    }
    $report = Report::create($report_data);
    $report->save();
    return $report_data;
  }

  public function storeImages(Request $request) {
    $path = $request->file('image')->store('images');
    return basename($path);
  }

  public function storeAudio(Request $request) {
    $report_data = $request->all();
    Log::info($report_data);
    $path = $request->file('audio')->store('audio');
    return $path;
  }


  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
    $report = Report::find($id);
    $this->removeImages($report->images);
    return $this->respond(Report::destroy($id));
  }

  private function removeImages($images) {
    $paths = [];
    foreach ($images as $image) {
      $filename = basename($image);
      $paths[] = ('/images/' . $filename);
      $paths[] = ('/images/thumbnails/' . $filename);
    }
    Storage::delete($paths);
  }
}
