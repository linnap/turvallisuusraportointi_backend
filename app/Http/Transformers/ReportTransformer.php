<?php

namespace App\Http\Transformers;

use App\Report;
use Illuminate\Support\Facades\Storage;
use League\Fractal;


class ReportTransformer extends Fractal\TransformerAbstract {

  public function transform(Report $report) {
    return [
      'id' => $report->id,
      'event' => $report->event,
      'images' => $this->getImagePaths($report->images),
      'text' => $report->text,
      'report_time' => $report->report_time,
      'user' => $report->user,
    ];
  }

  private function getImagePaths($images) {
    $data = [];
    foreach ($images as $image) {
      $data[] = ['thumb' => '/private/images/thumb/'.$image, 'full' => '/private/images/full/'.$image];
    }
    return $data;
  }


  
}