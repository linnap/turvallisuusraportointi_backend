<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
  protected $guarded = ['id'];

  protected $casts = [
    'event' => 'array',
    'images' => 'array',
    'user' => 'array'
  ];
}
