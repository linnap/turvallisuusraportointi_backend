<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** For mobile */
Route::middleware(['auth.apikey'])->group(function () {
  Route::post('/v1/report/save', 'ReportController@store');
  Route::post('/v1/report/save/images', 'ReportController@storeImages');
  Route::post('/v1/report/save/audio', 'ReportController@storeAudio');
});
